﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11
{
    interface IPayable
    {
        void Pay(double amount);
    }
}
