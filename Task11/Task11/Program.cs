﻿using System;
using System.Collections.Generic;

namespace Task11
{
    class Program
    {
        static void Main(string[] args)
        {

            bool customerFound = false;


            // Initialize customers in the system
            List<Customer> customers = new List<Customer>
            {
                new Customer(1, "bob", new CreditCard(), 111, new Cash(new List<Note> { new Note(50), new Note(100), new Note(1000) }, new List<Coin> { new Coin(1), new Coin(5), new Coin(20) })),
                new Customer(2, "john", new SavingsCard(1000), 250, new Cash(new List<Note> { new Note(100), new Note(100), new Note(1000) }, new List<Coin> { new Coin(5), new Coin(5), new Coin(5) })),
                new Customer(3, "billy", new SavingsCard(10), 100, new Cash(new List<Note> { new Note(50) }, new List<Coin> { new Coin(20), new Coin(5), new Coin(20) })),
                new Customer(4, "jessica", new CreditCard(), 100, new Cash(new List<Note> { new Note(50), new Note(100), new Note(500) }, new List<Coin> { new Coin(1), new Coin(1), new Coin(20) }))
            };

            // Prompt for userID to connect to the specific customer
            Console.WriteLine("Welcome, please enter your customer ID: (debug - valid values: 1,2,3,4)");
            int id = Int32.Parse(Console.ReadLine());
            Console.WriteLine();

            foreach (Customer customer in customers)
            {
                if(customer.CustomerID == id)                               // Customer found
                {

                    customerFound = true;

                    Console.WriteLine($"Hello, {customer.Name}");
                    Console.WriteLine($"You owe: {customer.AmountOwed}");
                    Console.WriteLine("______________________________________________________________");

                    Input:
                    Console.WriteLine("How do you wish to pay?");
                    Console.WriteLine("Enter 1 for Cash");
                    Console.WriteLine("Enter 2 Card (will use the card you have registered)");

                    string paymentChoice = Console.ReadLine();
                    Console.WriteLine();

                    // Find the customers desired payment choice. Call pay() method
                    if (paymentChoice.Equals("1"))
                    {
                        try
                        {
                            customer.Cash.Pay(customer.AmountOwed);
                        }
                        catch
                        {
                            Console.WriteLine("Not enough funds to cover your bill, please use another payment method");
                            Console.WriteLine("______________________________________________________________");
                            goto Input;
                        }  
                    }
                    else if (paymentChoice.Equals("2"))
                    {
                        try
                        {
                            customer.Card.Pay(customer.AmountOwed);
                            customer.AmountOwed = 0;
                        }
                        catch
                        {
                            Console.WriteLine("Not enough funds. Please try another payment method");
                        }                       
                    }
                    else
                    {
                        Console.WriteLine("Did not recognize input. Please try again");
                        goto Input;
                    }
                }
            }
            if (!customerFound)
            {
                Console.WriteLine("You do not appear to have an account in our system. Please register before using our services"); ;
            }
        }
    }
}
