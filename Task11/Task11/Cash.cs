﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11
{
    class Cash : IPayable
    {

        public List<Note> Notes { get; set; }
        public List<Coin> Coins { get; set; }
        private int cashTotal;

        public Cash(List<Note> notes, List<Coin> coins)
        {
            Notes = notes;
            Coins = coins;

            // Calculate total amount of cash
            foreach(Note n in notes)
            {
                cashTotal += n.Value;
            }
            foreach (Coin c in coins)
            {
                cashTotal += c.Value;
            }
        }


        public void Pay(double amount)
        {
            if((double)cashTotal >= amount)
            {
                // Sort notes and coins after value
                Notes.Sort();
                Coins.Sort();


                // Pay with available funds. Will use largest notes/coins first
                while(amount > 0 && Notes.Count > 0)
                {
                    amount -= Notes[0].Value;
                    Console.WriteLine($"Paying with note. Value of: {Notes[0].Value}");
                    Notes.RemoveAt(0);
                }
                while (amount > 0 && Coins.Count > 0)
                {
                    amount -= Coins[0].Value;
                    Console.WriteLine($"Paying with coin. Value of: {Coins[0].Value}");
                    Coins.RemoveAt(0);
                }

                amount *= -1; // Make change positive


                Console.WriteLine($"Payment with cash complete - your total change is: {amount}");


                // Calculate and display change for the customer
                List<Note> noteChange = new List<Note>();
                List<Coin> coinChange = new List<Coin>();

                int[] noteValues = { 1000, 500, 200, 100, 50 };   
                int[] coinValues = { 20, 10, 5, 1 };

                foreach (int val in noteValues)
                {
                    while(amount / (double) val >= 1)
                    {
                        noteChange.Add(new Note(val));
                        amount -= val;
                    }
                }
                foreach (int val in coinValues)
                {
                    while (amount / (double) val >= 1)
                    {
                        coinChange.Add(new Coin(val));
                        amount -= val;
                    }
                }
 
                foreach (Note n in noteChange)
                {
                    Console.WriteLine($"Note: {n.Value}");
                }
                foreach (Coin c in coinChange)
                {
                    Console.WriteLine($"Coin: {c.Value}");
                }
                
            }
            else
            {
                throw new Exception();
            }
        }
    }
}
