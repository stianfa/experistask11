﻿using System;

namespace Task11
{
    class SavingsCard : IPayable
    {
        private double balance;

        public SavingsCard(double balance)
        {
            this.balance = balance;
        }

        public void Pay(double amount)
        {
            if(amount > balance)
            {
                throw new Exception();
            }
            else
            {
                balance -= amount;
                Console.WriteLine("Payment complete");
            }
        }
    }
}
