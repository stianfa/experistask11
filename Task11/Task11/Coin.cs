﻿using System;

namespace Task11
{

    class Coin : IComparable
    {

        public int Value { get; }

        public Coin(int value)
        {
            if(value == 1 || value == 5 || value == 10 || value == 20)
            {
                Value = value;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public int CompareTo(object obj)
        {
            Coin c = (Coin)obj;
           if(this.Value > c.Value)
           {
                return -1;
           }
           else if(this.Value == c.Value)
            {
                return 0;
            }

            return 1;
        }
    }
}
