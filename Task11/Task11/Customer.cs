﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11
{
    class Customer
    {
       public int CustomerID { get; set; }
       public string Name { get; set; }
       public IPayable Card { get; set; }
       public double AmountOwed { get; set; }
       public Cash Cash { get; set; }
        
        public Customer(int customerID, string name, IPayable card, double amountOwed, Cash cash)
        {
            CustomerID = customerID;
            Name = name;
            Card = card;
            AmountOwed = amountOwed;
            Cash = cash;
        }
            
    }
}
