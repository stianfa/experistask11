﻿using System;

namespace Task11
{
    class Note : IComparable
    {
        public int Value { get; }

        public Note(int value)
        {
            if(value == 50 || value == 100 || value == 200 || value == 500 || value == 1000)
            {
                Value = value;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public int CompareTo(object obj)
        {
            Note n = (Note)obj;
            if (this.Value > n.Value)
            {
                return -1;
            }
            else if (this.Value == n.Value)
            {
                return 0;
            }

            return 1;
        }
    }
}
