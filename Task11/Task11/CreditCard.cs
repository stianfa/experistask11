﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11
{
    class CreditCard : IPayable
    {
        public void Pay(double amount)
        {
            Console.WriteLine("Payment complete: Credit card company billed!");
        }
    }
}
